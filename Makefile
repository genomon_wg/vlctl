CC = fccpx

CFLAGS =

OBJS = vlctl.o vl_test.o

all: vlctl vl_test

vlctl: vlctl.o
	$(CC) $(CFLAGS) vlctl.o -o $@

vl_test: vl_test.o
	$(CC) $(CFLAGS) vl_test.o -o $@

.c.o:
	$(CC) $(CFLAGS) -c $<

clean:
	rm -f $(OBJS)

distclean: clean
	rm -f vlctl vl_test

